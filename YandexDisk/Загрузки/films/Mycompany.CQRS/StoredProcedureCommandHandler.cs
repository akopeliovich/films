﻿using System.Diagnostics.Contracts;
using System.Threading.Tasks;

namespace Mycompany.CQRS {
	public class StoredProcedureCommandHandler<TCommand> : ICommandHandler<TCommand>
		where TCommand : ICommand {
		private readonly IStoredProcedureNameResolver _storedProcedureNameResolver;
		private readonly IDbParametersResolver _dbParametersResolver;
		private readonly IStoredProcedureExecutor _storedProcedureExecutor;

		public StoredProcedureCommandHandler(
			IStoredProcedureNameResolver storedProcedureNameResolver,
			IDbParametersResolver dbParametersResolver,
			IStoredProcedureExecutor storedProcedureExecutor) {
			Contract.Requires(storedProcedureNameResolver != null);
			Contract.Requires(dbParametersResolver != null);
			Contract.Requires(storedProcedureExecutor != null);

			_storedProcedureNameResolver = storedProcedureNameResolver;
			_dbParametersResolver = dbParametersResolver;
			_storedProcedureExecutor = storedProcedureExecutor;
		}

		public async Task HandleAsync(TCommand command) {
			await Task.Run(() => {
				_storedProcedureExecutor.ExecuteScalar(
					GetStoredProcedureName(command), 
					_dbParametersResolver.GetParams(command)
				);
			}).ConfigureAwait(false);
		}

		private string GetStoredProcedureName(TCommand command) {
			return _storedProcedureNameResolver.GetProcedureName(command);
		}
	}
}