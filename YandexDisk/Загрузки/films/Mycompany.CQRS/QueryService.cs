﻿using System;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;

using Mycompany.DI;

namespace Mycompany.CQRS {
	public class QueryService : IQueryService {
		private readonly IMycompanyContainer _container;

		public QueryService(IMycompanyContainer container) {
			Contract.Requires(container != null);
			_container = container;
		}

		public async Task<TResult> QueryAsync<TResult>(IQuery<TResult> query) {
			var handler = ResolveHandler(query, typeof(IQueryHandler<,>));

			return await ((Task<TResult>)((dynamic)handler).HandleAsync((dynamic)query)).ConfigureAwait(false);
		}

		private object ResolveHandler<TResult>(IQuery<TResult> query, Type interfaceType) {
			var handlerType = interfaceType.MakeGenericType(query.GetType(), typeof(TResult));

			return _container.Resolve(handlerType);
		}
	}
}