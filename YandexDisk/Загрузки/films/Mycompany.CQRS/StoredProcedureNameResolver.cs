﻿using System.Diagnostics.Contracts;

namespace Mycompany.CQRS {
	public class StoredProcedureNameResolver : IStoredProcedureNameResolver {
		private readonly string _format;

		public StoredProcedureNameResolver(string format) {
			Contract.Requires(!string.IsNullOrEmpty(format));

			_format = format;
		}

		public string GetProcedureName(object query) {
			return string.Format(_format, query.GetType().Name);
		}
	}
}