﻿namespace Mycompany.CQRS {
	public interface IStoredProcedureNameResolver {
		string GetProcedureName(object query);
	}
}