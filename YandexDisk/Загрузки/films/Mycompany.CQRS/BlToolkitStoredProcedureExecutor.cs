﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.Contracts;

using BLToolkit.Data;

namespace Mycompany.CQRS {
	public class BlToolkitStoredProcedureExecutor : IStoredProcedureExecutor {
		private readonly IConectionProvider _conectionProvider;

		public BlToolkitStoredProcedureExecutor(IConectionProvider conectionProvider) {
			Contract.Requires(conectionProvider != null);

			_conectionProvider = conectionProvider;
		}

		public object ExecuteScalar(
			string storedProcedureName,
			IDbDataParameter[] parameters) {
			using (var connection = OpenNewConnection()) {
				using (var db = new DbManager(connection)) {
					return db.SetSpCommand(
						storedProcedureName,
						parameters
					).ExecuteScalar();
				}
			}
		}

		public ArrayList ExecuteList(
			string storedProcedureName,
			IDbDataParameter[] parameters,
			Type genericParameterType) {
			using (var connection = OpenNewConnection()) {
				using (var db = new DbManager(connection)) {
					return db.SetSpCommand(
						storedProcedureName,
						parameters
					).ExecuteList(genericParameterType);
				}
			}
		}

		private SqlConnection OpenNewConnection() {
			return _conectionProvider.OpenNewConnection();
		}
	}
}