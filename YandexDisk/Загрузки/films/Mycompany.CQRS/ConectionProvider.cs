﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics.Contracts;

namespace Mycompany.CQRS {
	public class ConectionProvider : IConectionProvider {
		private readonly string _connectionStringName;

		public ConectionProvider(string connectionStringName) {
			Contract.Requires(!string.IsNullOrEmpty(connectionStringName));

			_connectionStringName = connectionStringName;
		}

		public SqlConnection OpenNewConnection() {
			var connection = ConfigurationManager.ConnectionStrings[_connectionStringName];
			if (connection == null || string.IsNullOrEmpty(connection.ConnectionString)) {
				throw new Exception("Error: missing connecting string in web.config file");
			}

			var sqlConnection = new SqlConnection(connection.ConnectionString);
			sqlConnection.Open();

			return sqlConnection;
		}
	}
}