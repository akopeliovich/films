﻿using System.Data.SqlClient;

namespace Mycompany.CQRS {
	public interface IConectionProvider {
		SqlConnection OpenNewConnection();
	}
}