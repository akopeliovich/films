﻿using System.Threading.Tasks;

namespace Mycompany.CQRS {
	public interface IQueryService {
		Task<TResult> QueryAsync<TResult>(IQuery<TResult> query);
	}
}