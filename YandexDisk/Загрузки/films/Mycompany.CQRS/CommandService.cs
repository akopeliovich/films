﻿using System.Diagnostics.Contracts;
using System.Threading.Tasks;

using Mycompany.DI;

namespace Mycompany.CQRS {
	public class CommandService : ICommandService {
		private readonly IMycompanyContainer _container;

		public CommandService(IMycompanyContainer container) {
			Contract.Requires(container != null);

			_container = container;
		}

		public async Task HandleAsync<T>(T command) where T : ICommand {
			var commandType = typeof(ICommandHandler<>).MakeGenericType(command.GetType());
			var handler = _container.Resolve(commandType);

			await ((dynamic)handler).HandleAsync((dynamic)command);
		}
	}
}