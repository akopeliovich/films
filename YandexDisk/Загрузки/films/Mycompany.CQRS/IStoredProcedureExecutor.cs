﻿using System;
using System.Collections;
using System.Data;

namespace Mycompany.CQRS {
	public interface IStoredProcedureExecutor {
		object ExecuteScalar(
			string storedProcedureName,
			IDbDataParameter[] parameters);

		ArrayList ExecuteList(
			string storedProcedureName,
			IDbDataParameter[] parameters,
			Type genericParameterType);
	}
}