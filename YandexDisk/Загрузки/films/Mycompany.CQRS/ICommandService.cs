﻿using System.Threading.Tasks;

namespace Mycompany.CQRS {
	public interface ICommandService {
		Task HandleAsync<T>(T command) where T : ICommand;
	}
}