﻿using System.Data;

namespace Mycompany.CQRS {
	public interface IDbParametersResolver {
		IDbDataParameter[] GetParams(object query);
	}
}