﻿using System.Collections.Generic;
using System.Data;

using BLToolkit.Data;

namespace Mycompany.CQRS {
	public class DbParametersResolver : IDbParametersResolver {
		public IDbDataParameter[] GetParams(object query) {
			var manager = new DbManager();
			var patrameters = new List<IDbDataParameter>();

			foreach (var field in query.GetType().GetProperties()) {
				var value = field.GetValue(query);

				patrameters.Add(manager.Parameter("@" + field.Name, value));
			}

			return patrameters.ToArray();
		}
	}
}