﻿using System.Threading.Tasks;

namespace Mycompany.CQRS {
	public interface ICommandHandler<in T>
		where T : ICommand {
		Task HandleAsync(T command);
	}
}