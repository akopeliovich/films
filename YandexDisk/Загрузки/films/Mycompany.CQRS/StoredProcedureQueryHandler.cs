﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace Mycompany.CQRS {
	public class StoredProcedureQueryHandler<TQuery, TResult> : IQueryHandler<TQuery, TResult>
		where TQuery : IQuery<TResult> {
		private readonly IStoredProcedureNameResolver _storedProcedureNameResolver;
		private readonly IDbParametersResolver _dbParametersResolver;
		private readonly IStoredProcedureExecutor _storedProcedureExecutor;

		public StoredProcedureQueryHandler(
			IStoredProcedureNameResolver storedProcedureNameResolver,
			IDbParametersResolver dbParametersResolver,
			IStoredProcedureExecutor storedProcedureExecutor) {
			Contract.Requires(storedProcedureNameResolver != null);
			Contract.Requires(dbParametersResolver != null);
			Contract.Requires(storedProcedureExecutor != null);

			_storedProcedureNameResolver = storedProcedureNameResolver;
			_dbParametersResolver = dbParametersResolver;
			_storedProcedureExecutor = storedProcedureExecutor;
		}

		public async Task<TResult> HandleAsync(TQuery query) {
			var resultType = typeof(TResult);
			var storedProcedureName = GetStoredProcedureName(query);

			var genericParameterType = resultType.GenericTypeArguments.First();

			if (IsGenericEnumerableType(resultType)) {
				var objectResult = await Task.Run(
					() => _storedProcedureExecutor.ExecuteList(
						storedProcedureName,
						_dbParametersResolver.GetParams(query),
						genericParameterType)
					)
					.ConfigureAwait(false);

				return EnumerableObjectResutlToTResult(objectResult, genericParameterType);
			}

			return await Task.Run(
				() => (TResult)_storedProcedureExecutor
					.ExecuteScalar(storedProcedureName, _dbParametersResolver.GetParams(query)))
					.ConfigureAwait(false);
		}

		private TResult EnumerableObjectResutlToTResult(ArrayList objectResult, Type genericParameterType) {
			var result = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(genericParameterType));

			foreach (var item in objectResult) {
				result.Add(Convert.ChangeType(item, genericParameterType));
			}

			return (TResult)result;
		}

		private bool IsGenericEnumerableType(Type resultType) {
			return resultType.IsGenericType && resultType.GetGenericTypeDefinition() == typeof(IEnumerable<>);
		}

		private string GetStoredProcedureName(TQuery query) {
			return _storedProcedureNameResolver.GetProcedureName(query);
		}
	}
}