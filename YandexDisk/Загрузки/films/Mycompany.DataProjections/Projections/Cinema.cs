﻿namespace Mycompany.DataProjections.Projections {
	public class Cinema {
		public int Id { get; set; }

		public string Title { get; set; }
	}
}