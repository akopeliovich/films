﻿namespace Mycompany.DataProjections.Projections {
	public class Film {
		public int Id { get; set; }

		public string Title { get; set; }
	}
}