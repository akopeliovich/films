﻿using System;

namespace Mycompany.DataProjections.Projections {
	public class Schedule {
		public Film Film { get; set; }

		public Cinema Cinema { get; set; }

		public DateTime Date { get; set; }
	}
}