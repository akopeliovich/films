﻿using System;
using System.Collections.Generic;
using System.Linq;

using FluentValidation.Attributes;

namespace Mycompany.Web.Models.Home {
	[Validator(typeof(ScheduleFormValidator))]
	public class ScheduleForm {
		public ScheduleForm() {
			Times = Enumerable.Empty<DateTime>();
		}

		public int CinemaId { get; set; }

		public int FilmId { get; set; }

		public DateTime Date { get; set; }

		public IEnumerable<DateTime> Times { get; set; }
	}
}