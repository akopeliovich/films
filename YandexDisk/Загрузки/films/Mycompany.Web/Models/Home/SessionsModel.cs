﻿using System;
using System.Collections.Generic;

using MyCompany.Domain.Projections;
using MyCompany.Domain.Queries;

namespace Mycompany.Web.Models.Home {
	public class SessionsModel {
		public IEnumerable<Film> Films { get; set; }

		public IEnumerable<Cinema> Cinemas { get; set; }

		public IEnumerable<Session> Sessions { get; set; }

		public int? CinemaId { get; set; }

		public int? FilmId { get; set; }

		public DateTime? Date { get; set; }
	}
}