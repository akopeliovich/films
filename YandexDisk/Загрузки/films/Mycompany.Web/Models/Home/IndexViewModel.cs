﻿using System;
using System.Collections.Generic;

using MyCompany.Domain.Projections;

namespace Mycompany.Web.Models.Home {
	public class IndexViewModel {
		public IList<Schedule> Schedule { get; set; }

		public IList<DateTime> Dates { get; set; }

		public DateTime Date { get; set; }
	}
}