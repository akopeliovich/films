﻿using System;
using System.Collections.Generic;

using FluentValidation;

namespace Mycompany.Web.Models.Home {
	public class ScheduleFormValidator : AbstractValidator<ScheduleForm> {
		public ScheduleFormValidator() {
			RuleFor(t => t.Times).Must(t => !HasDuplicates(t)).WithMessage("Несколько одинаковых сеансов");
			RuleFor(t => t.FilmId).NotNull().WithMessage("Укажите фильм");
			RuleFor(t => t.CinemaId).NotNull().WithMessage("Укажите кинотеатр");
			RuleFor(t => t.Date).NotNull().WithMessage("Укажите дату");
		}

		private bool HasDuplicates(IEnumerable<DateTime> dates) {
			var vals = new List<DateTime>();
			var returnValue = false;
			foreach (var date in dates) {
				if (vals.Contains(date)) {
					returnValue = true;
					break;
				}

				vals.Add(date);
			}

			return returnValue;
		}
	}
}