﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

using Mycompany.CQRS;
using Mycompany.Web.Models.Home;

using MyCompany.Domain.Commands;
using MyCompany.Domain.Queries;

namespace Mycompany.Web.Controllers {
	public class HomeController : Controller {
		private readonly ICommandService _commandService;
		private readonly IQueryService _queryService;

		public HomeController(ICommandService commandService, IQueryService queryService) {
			Contract.Requires(commandService != null);
			Contract.Requires(queryService != null);

			_commandService = commandService;
			_queryService = queryService;

		}

		public async Task<ActionResult> Index(DateTime? date = null) {
			var model = new IndexViewModel();
			date = date ?? DateTime.Now.Date;

			model.Schedule = (await _queryService.QueryAsync(new GetSchedule(date.Value))).ToList();

			model.Date = date.Value;

			model.Dates = GenerateDates();
			model.Dates.Add(date.Value);

			return View(model);
		}

		[HttpGet]
		public async Task<ActionResult> Sessions(DateTime? date, int? cinemaId, int? filmId) {
			var films = _queryService.QueryAsync(new GetFilms());
			var cinemas = _queryService.QueryAsync(new GetCinemas());
			var sessions = _queryService.QueryAsync(new GetSessions(date ?? DateTime.Now, cinemaId, filmId));

			await Task.WhenAll(films, cinemas, sessions);

			var model = new SessionsModel {
				Films = films.Result,
				Cinemas = cinemas.Result,
				Sessions = sessions.Result,
				CinemaId = cinemaId,
				FilmId = filmId,
				Date = date
			};

			return View(model);
		}

		[HttpPost]
		public async Task<ActionResult> SaveSchedule(ScheduleForm scheduleForm) {
			if (!ModelState.IsValid) {
				var films = _queryService.QueryAsync(new GetFilms());
				var cinemas = _queryService.QueryAsync(new GetCinemas());
				
				await Task.WhenAll(films, cinemas);

				var model = new SessionsModel {
					Films = films.Result,
					Cinemas = cinemas.Result,
					Sessions = scheduleForm.Times.Select(t => new Session {
							Date = t
						}),
					CinemaId = scheduleForm.CinemaId,
					FilmId = scheduleForm.FilmId,
					Date = scheduleForm.Date
				};

				return View("sessions", model);
			}

			await _commandService.HandleAsync(new SaveSchedule(
				scheduleForm.Times.Select(t => t).ToArray(),
				scheduleForm.FilmId,
				scheduleForm.CinemaId));

			return new RedirectResult("index");
		}

		public ActionResult Contact() {
			return View();
		}

		private List<DateTime> GenerateDates() {
			var start = DateTime.Now.AddDays(-10);
			var end = DateTime.Now.AddDays(10);

			return Enumerable
				.Range(0, 1 + end.Subtract(start).Days)
				.Select(offset => start.AddDays(offset))
				.ToList();
		}
	}
}