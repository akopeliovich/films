﻿(function () {
    'use strict';

    function sessions(options) {
        var addBtn = document.getElementById(options['add-button-id']),
            dellBtn = document.getElementById(options['delete-button-id']),
            timeList = document.getElementById(options['time-list-id']),
            timeListHidden = document.getElementById(options['time-list-h-id']),
            inputDate = document.getElementById(options['date-input-id']),
            newTime = document.getElementById(options['new-time-input-id']),
            selectedCinemaId = document.getElementById(options['select-sinema-id']),
            selectedFilmId = document.getElementById(options['select-film-id']),
            query = {
                cinemaId: '',
                filmId: '',
                date: '',
                toQueryString: function () {
                    return '?date=' + this.date + '&cinemaId=' + this.cinemaId + '&filmId=' + this.filmId;
                }
            };

        query.date = inputDate.value;

        if (window.location.search) {
            query.cinemaId = selectedCinemaId.value;
            query.filmId = selectedFilmId.value;
        }

        function addToTimeList(value) {
            var option = document.createElement('option');
            option.innerHTML = value;
            timeList.appendChild(option);
        }

        function addToTimeListHidden(value) {
            var hidden = document.createElement('input');
            hidden.name = 'Times';
            hidden.type = 'hidden';
            hidden.value = inputDate.value + ' ' + value;
            timeListHidden.appendChild(hidden);
        }


        function addClick() {
            var value = newTime.value;

            if (!value) {
                return;
            }

            addToTimeList(value);
            addToTimeListHidden(value);

            newTime.value = '';
        }

        function dellClick() {
            var options = timeList.getElementsByTagName('option'),
                i = options.length;

            while (i--) {
                if (options[i].selected) {
                    timeListHidden.removeChild(timeListHidden.children[i]);
                    timeList.removeChild(options[i]);
                    break;
                }
            }
        }
        
        function redirect() {
            if (!query.cinemaId || !query.filmId || !query.date) {
                return;
            }

            window.location.href = options.redirectUrl + query.toQueryString();
        }
        
        selectedCinemaId.addEventListener('change', function () {
            query.cinemaId = this.value;
            redirect();
        });

        selectedFilmId.addEventListener('change', function () {
            query.filmId = this.value;
            redirect();
        });

        inputDate.addEventListener('change', function () {
            var date = new Date(this.value);

            function addZero(value) {
                return value > 10 ? value : '0' + value;
            }

            query.date = addZero(date.getDate()) + '.' + addZero((date.getMonth() + 1)) + '.' + date.getFullYear();
            redirect();
        });

        addBtn.addEventListener('click', addClick);
        dellBtn.addEventListener('click', dellClick);
    }

    window.app = {
        sessions: sessions
    }
}());