﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using Autofac.Integration.Mvc;

using FluentValidation.Mvc;

using Mycompany.Web.DI;

namespace Mycompany.Web {
	public class MvcApplication : HttpApplication {
		protected void Application_Start() {
			AreaRegistration.RegisterAllAreas();

			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			AuthConfig.RegisterAuth();
			DependencyResolver.SetResolver(new AutofacDependencyResolver(MycompanyContainerConfigurator.GetContainer()));

			FluentValidationModelValidatorProvider.Configure();

			ModelBinders.Binders.Add(typeof(DateTime), new DateTimeBinder());
			ModelBinders.Binders.Add(typeof(DateTime?), new NullableDateTimeBinder());
		}
	}
}