﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace Mycompany.Web {
	public class NullableDateTimeBinder : IModelBinder {
		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {
			var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
			if (value != null) {
				var date = value.ConvertTo(typeof(DateTime), CultureInfo.CurrentCulture);
				return date;
			}

			return null;
		}
	}
}