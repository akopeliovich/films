﻿using System;
using System.Diagnostics.Contracts;

using Autofac;

using Mycompany.DI;

namespace Mycompany.Web.DI {
	public class MycompanyContainer : IMycompanyContainer {
		private readonly IComponentContext _componentContext;

		public MycompanyContainer(IComponentContext componentContext) {
			Contract.Requires(componentContext != null);

			_componentContext = componentContext;
		}

		public object Resolve(Type serviceType) {
			return _componentContext.Resolve(serviceType);
		}
	}
}