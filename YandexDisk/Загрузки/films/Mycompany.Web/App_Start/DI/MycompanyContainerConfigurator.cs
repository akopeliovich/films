﻿using System;
using System.Reflection;

using Autofac;
using Autofac.Integration.Mvc;

using Mycompany.CQRS;
using Mycompany.DI;

namespace Mycompany.Web.DI {
	public class MycompanyContainerConfigurator {
		private static readonly Lazy<MycompanyContainerConfigurator> _instance = new Lazy<MycompanyContainerConfigurator>(
			() => new MycompanyContainerConfigurator(),
			true
		);
		
		public readonly Lazy<IContainer> Container = new Lazy<IContainer>(
			() => _instance.Value._builder.Build(),
			true
		);

		public static IContainer GetContainer() {
			return _instance.Value.Container.Value;
		}

		private readonly ContainerBuilder _builder;

		private MycompanyContainerConfigurator() {
			_builder = new ContainerBuilder();

			RegisterInterfaces();

			RegisterControllers();

			RegisterQueryHandlers();

			RegisterCommandHandlers();
		}

		private void RegisterInterfaces() {
			_builder.RegisterInstance<Func<IContainer>>(GetContainer);
			_builder.Register<IMycompanyContainer>(t => new MycompanyContainer(t.Resolve<Func<IContainer>>().Invoke()));

			_builder.RegisterType<QueryService>().As<IQueryService>();
			_builder.RegisterType<CommandService>().As<ICommandService>();
			_builder.RegisterType<DbParametersResolver>().As<IDbParametersResolver>();
			_builder.RegisterType<BlToolkitStoredProcedureExecutor>().As<IStoredProcedureExecutor>();
			_builder.RegisterType<ConectionProvider>().As<IConectionProvider>().WithParameter(new NamedParameter("connectionStringName", "DefaultConnection"));

			_builder
				.RegisterType<StoredProcedureNameResolver>()
				.As<IStoredProcedureNameResolver>()
				.WithParameter(new NamedParameter("format", "[dbo].[Mycompany.{0}]"));
		}

		private void RegisterControllers() {
			_builder.RegisterControllers(typeof(MvcApplication).Assembly);
		}

		private void RegisterQueryHandlers() {
			_builder.RegisterGeneric(typeof(StoredProcedureQueryHandler<,>)).As(typeof(IQueryHandler<,>));

			_builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
				.AsClosedTypesOf(typeof(IQueryHandler<,>));
		}

		private void RegisterCommandHandlers() {
			_builder.RegisterGeneric(typeof(StoredProcedureCommandHandler<>)).As(typeof(ICommandHandler<>));

			_builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).AsClosedTypesOf(typeof(ICommandHandler<>));
		}
	}
}