﻿using System;

namespace MyCompany.Domain.Projections {
	public class Schedule {
		public Film Film { get; set; }

		public Cinema Cinema { get; set; }

		public DateTime Date { get; set; }
	}
}