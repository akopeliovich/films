﻿using System;
using System.Collections.Generic;

using Mycompany.CQRS;

namespace MyCompany.Domain.Queries {
	public class GetSessions : IQuery<IEnumerable<Session>> {
		public GetSessions(DateTime date, int? cinemaId = null, int? filmId = null) {
			Date = date;
			CinemaId = cinemaId;
			FilmId = filmId;
		}

		public DateTime Date { get; }

		public int? CinemaId { get; }

		public int? FilmId { get; }
	}
}