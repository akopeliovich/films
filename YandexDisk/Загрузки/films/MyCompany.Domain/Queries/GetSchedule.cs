﻿using System;
using System.Collections.Generic;

using Mycompany.CQRS;

using MyCompany.Domain.Projections;

namespace MyCompany.Domain.Queries {
	public class GetSchedule : IQuery<IEnumerable<Schedule>> {
		public GetSchedule(DateTime date, int? cinemaId = null, int? filmId = null) {
			Date = date;
			CinemaId = cinemaId;
			FilmId = filmId;
		}

		public DateTime Date { get; }

		public int? CinemaId { get; }

		public int? FilmId { get; }
	}
}