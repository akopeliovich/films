﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

using Mycompany.CQRS;

namespace MyCompany.Domain.Commands {
	public class SaveSchedule : ICommand {
		public SaveSchedule(DateTime[] dates, long filmId, long cinemaId) {
			Contract.Requires(dates != null);
			Contract.Requires(dates.Any());

			Dates = dates;
			FilmId = filmId;
			CinemaId = cinemaId;
		}

		public DateTime[] Dates { get; }

		public long FilmId { get; }

		public long CinemaId { get; }
	}
}