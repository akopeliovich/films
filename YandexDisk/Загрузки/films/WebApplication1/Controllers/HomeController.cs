﻿
using System;
using System.Linq;
using System.Web.Mvc;

using BLToolkit.Reflection;

using Mycompany.Core.Data;

namespace WebApplication1.Controllers {
	public class HomeController : Controller {
		public ActionResult Index() {
			var pa = TypeAccessor<ScheduleAccessor>.CreateInstance();
			var list = pa.GetSchedule(new DateTime(1990, 12, 23),null, null ).ToList();

			return View();
		}

		public ActionResult About() {
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact() {
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}