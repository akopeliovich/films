﻿using System;

namespace Mycompany.DI {
	public interface IMycompanyContainer {
		object Resolve(Type serviceType);
	}
}