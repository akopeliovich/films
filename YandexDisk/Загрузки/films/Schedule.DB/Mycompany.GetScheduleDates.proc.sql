﻿create procedure [dbo].[Mycompany.GetSessions](
	@date [datetime],
	@cinemaId [int],
	@filmId [int]
) as begin
	declare @startDate [datetime] = cast(@date as date)
	declare @endDate [datetime] = cast(dateadd(day, 1, @startDate) as date)

	select [Date]
	from [dbo].[Mycompany.Schedule]
	where [Date] between @startDate and @endDate
		and (@cinemaId is null or [CinemaId] = @cinemaId)
		and (@filmId is null or [FilmId] = @filmId)
	order by [Date] desc
end