﻿create table [dbo].[Mycompany.Cinemas](
	[Id] [int] identity (1, 1) not null,
	[Title] [nvarchar](255) not null
)
go

alter table [dbo].[Mycompany.Cinemas]
	add constraint [Mycompany.Cinemas.PrimaryKey] 
	primary key clustered ([Id])
go