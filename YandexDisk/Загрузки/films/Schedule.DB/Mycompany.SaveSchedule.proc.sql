﻿create procedure [dbo].[Mycompany.SaveSchedule](
	@dates [dbo].[DateTable] readonly,
	@filmId [int],
	@cinemaId [int]
) as begin
	merge [dbo].[Mycompany.Schedule] with(rowlock, holdlock) as [target] 
	using (
		select 
			[FilmId] = @filmId,
			[CinemaId] = @cinemaId,
			[Date]
		from @dates
	) as [source]
		on [target].[FilmId] = [source].[FilmId]
			and [target].[CinemaId] = [source].[CinemaId]
			and [target].[Date] = [source].[Date]	
	when not matched by target then
		insert (
			[FilmId],
			[CinemaId],
			[Date]
		)
		values(		
			[source].[FilmId],
			[source].[CinemaId],
			[source].[Date]
		)
	when not matched by source
		and [target].[FilmId] = @filmId
		and [target].[CinemaId] = @cinemaId
		then delete;
end