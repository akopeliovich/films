﻿create table [dbo].[Mycompany.Schedule] (
	[CinemaId] [int] not null,
	[FilmId] [int] not null,
	[Date] [datetime] not null
)
go 

alter table [dbo].[Mycompany.Schedule]
	add constraint [Mycompany.Schedule.PrimaryKey] 
	primary key clustered ([FilmId], [CinemaId], [Date])
go

alter table [dbo].[Mycompany.Schedule]
	add constraint [Mycompany.Schedule.FilmKey]
	foreign key ([FilmId])
	references [dbo].[Mycompany.Films]([Id])
go

alter table [dbo].[Mycompany.Schedule]
	add constraint [Mycompany.Schedule.CinemaKey]
	foreign key ([CinemaId])
	references [dbo].[Mycompany.Cinemas]([Id])
go

create nonclustered index [Mycompany.Schedule.Date]
	on [dbo].[Mycompany.Schedule] ([Date])
go