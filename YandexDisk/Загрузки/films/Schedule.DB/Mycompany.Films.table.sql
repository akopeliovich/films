﻿create table [dbo].[Mycompany.Films] (
	[Id] [int] identity (1, 1) not null,
	[Title] [nvarchar](255) not null
)
go 

alter table [dbo].[Mycompany.Films]
	add constraint [Mycompany.Films.PrimaryKey] 
	primary key clustered ([Id])
go