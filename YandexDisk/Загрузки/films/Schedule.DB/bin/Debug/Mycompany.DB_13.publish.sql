﻿/*
Deployment script for Mycompany

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "Mycompany"
:setvar DefaultFilePrefix "Mycompany"
:setvar DefaultDataPath "c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\"
:setvar DefaultLogPath "c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Creating [dbo].[SaveSchedule]...';


GO
create procedure [dbo].[SaveSchedule](
	@items [dbo].[Mycompany.ScheduleItem] readonly
) as begin
	merge [dbo].[Mycompany.Schedule] with(rowlock, holdlock) as [target] 
	using (
		select 
			[FilmId],
			[CinemaId],
			[Date]
		from @items
	) as [source]
		on [target].[FilmId] = [source].[FilmId]
			and [target].[CinemaId] = [source].[CinemaId]
			and [target].[Date] = [source].[Date]	
	when not matched by target then
		insert (
			[FilmId],
			[CinemaId],
			[Date]
		)
		values(		
			[source].[FilmId],
			[source].[CinemaId],
			[source].[Date]
		)
	when not matched by source 
		then delete;
end
GO
PRINT N'Update complete.';


GO
