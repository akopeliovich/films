﻿/*
Deployment script for Mycompany

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "Mycompany"
:setvar DefaultFilePrefix "Mycompany"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Creating [dbo].[Schedule_GetSchedule]...';


GO
create procedure [dbo].[Schedule_GetSchedule](
	@date [datetime],
	@cinemaId [int],
	@filmId [int]
) as begin
	declare @startDate [datetime] = cast(@date as date)
	declare @endDate [datetime] = cast(dateadd(day, 1, @startDate) as date)
	
	select
		[cinema].[Title],
		[film].[Title],
		[schedule].[Date],
		[schedule].[FilmId],
		[schedule].[CinemaId]
	from [dbo].[Mycompany.Schedule] as [schedule]
		inner join [dbo].[Mycompany.Cinemas] as [cinema]
			on [cinema].[Id] = [schedule].[CinemaId]
		inner join [dbo].[Mycompany.Films] as [film]
			on [film].[Id] = [schedule].[FilmId]
	where [schedule].[Date] between @startDate and @endDate
		and (@cinemaId is null or [schedule].[CinemaId] = @cinemaId)
		and (@filmId is null or [schedule].[FilmId] = @cinemaId)
end
GO
PRINT N'Update complete.';


GO
