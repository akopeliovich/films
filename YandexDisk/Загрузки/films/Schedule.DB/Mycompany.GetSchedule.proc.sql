﻿create procedure [dbo].[Mycompany.GetSchedule](
	@date [datetime],
	@cinemaId [int],
	@filmId [int]
) as begin
	declare @startDate [datetime] = cast(@date as date)
	declare @endDate [datetime] = cast(dateadd(day, 1, @startDate) as date)
	
	select
		[Cinema.Id] = [schedule].[CinemaId],
		[Cinema.Title] = [cinema].[Title],
		[Film.Id] = [schedule].[FilmId],
		[Film.Title] = [film].[Title],
		[schedule].[Date]
	from [dbo].[Mycompany.Schedule] as [schedule]
		inner join [dbo].[Mycompany.Cinemas] as [cinema]
			on [cinema].[Id] = [schedule].[CinemaId]
		inner join [dbo].[Mycompany.Films] as [film]
			on [film].[Id] = [schedule].[FilmId]
	where [schedule].[Date] between @startDate and @endDate
		and (@cinemaId is null or [schedule].[CinemaId] = @cinemaId)
		and (@filmId is null or [schedule].[FilmId] = @filmId)
	order by [schedule].[CinemaId], [schedule].[FilmId]
end